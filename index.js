const {readFileSync} = require('fs')

const inputFile = process.argv[2]

if (!inputFile) process.exit(1)

const data = readFileSync(inputFile, 'utf8')

const lines = data.split('\n')

const colors = {}

// sort by length
lines.sort((a, b) => a.length - b.length);

const numberOfColors = parseInt(lines[0])

for (var i = 1; i <= numberOfColors; i++) {
    colors[i] = 0
}

// each line
lines.forEach((line, index) => {
    if (index) {
        let arrLine = [];
        for (let i = 0; i <= line.length; i += 2) {
            if (!isNaN(parseInt(line[i], 10)))
                arrLine.push(line[i + 2] + line[i])
        }

        //1 M 3 G 5 G => G3 G5 M1
        arrLine.sort((a, b) => a.localeCompare(b));
        //console.log('arrLine',arrLine)


        for (let i = 0; i < arrLine.length; i++) {
            let colorNumber = arrLine[i].charAt(1)
            let colorType = arrLine[i].charAt(0)

            if (!(colors[colorNumber]) ||
                (colors[colorNumber] === colorType)) {
                colors[colorNumber] = colorType;
                break;
            } else if (i == arrLine.length - 1) {
                console.log('No solution exists')
                process.exit(1)
            }
        }
        //console.log(colors)

    }
    // end else

})

Object.keys(colors).forEach((color, index) => {
    if (!colors[index + 1]) colors[index + 1] = 'G'
})

console.log(Object.values(colors).join(' '))
